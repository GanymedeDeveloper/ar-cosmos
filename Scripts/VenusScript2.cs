using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VenusScript2 : MonoBehaviour, IVirtualButtonEventHandler
{

    public GameObject vBtObject;
    public GameObject Model;

    // Start is called before the first frame update
    void Start()
    {
       // vBtObject = GameObject.Find("VenusVB");
        vBtObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

        Model = GameObject.Find("venus");
        Model.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Model.SetActive(true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Model.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
