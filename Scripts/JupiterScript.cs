using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class JupiterScript : MonoBehaviour, IVirtualButtonEventHandler
{

    public GameObject vBtObject;
    public GameObject Ganymede;
    public GameObject IO;
    public GameObject Europe;
    public GameObject Callisto;

    // Start is called before the first frame update
    void Start()
    {
        // vBtObject = GameObject.Find("VenusVB");
        vBtObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

        Ganymede = GameObject.Find("Ganymede");
        Ganymede.SetActive(false);

        IO = GameObject.Find("IO");
        IO.SetActive(false);

        Europe = GameObject.Find("Europe");
        Europe.SetActive(false);

        Callisto = GameObject.Find("Callisto");
        Callisto.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Ganymede.SetActive(true);
        IO.SetActive(true);
        Europe.SetActive(true);
        Callisto.SetActive(true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Ganymede.SetActive(false);
        IO.SetActive(false);
        Europe.SetActive(false);
        Callisto.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

    }

}