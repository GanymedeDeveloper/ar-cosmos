using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class NeptuneScript : MonoBehaviour, IVirtualButtonEventHandler
{
    public GameObject vBtObject1;
    public GameObject vBtObject2;

    public GameObject Model;
    public double x = 0.0001;

// Start is called before the first frame update
void Start()
{
        vBtObject1.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        vBtObject2.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);


        Model = GameObject.Find("Neptune");
        Model.SetActive(true);
}

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        switch (vb.VirtualButtonName)
        {
            case "NB1":
                x = x + 0.00005;
                float y = (float)x;
                Model.transform.localScale = new Vector3(y, y, y);
                break;
            case "NB2":
                x = x - 0.00005;
                float z = (float)x;
                Model.transform.localScale = new Vector3(z, z, z);
                break;
            default:
                throw new UnityException("Button not supported: " + vb.VirtualButtonName);
                break;
        }

        
    }

    

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
      
    }


    // Update is called once per frame
    void Update()
{
        

}
}