using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SaturnScript : MonoBehaviour, IVirtualButtonEventHandler
{
    public GameObject vBtObject1;

    public GameObject Model;
    

    // Start is called before the first frame update
    void Start()
    {
        vBtObject1.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
       
        Model = GameObject.Find("Ring");
        Model.SetActive(true);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Model.SetActive(false);

    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Model.SetActive(true);
    }


    // Update is called once per frame
    void Update()
    {


    }
}